#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

int scanInt() {
    int value;

    scanf("%d", &value);

    return value;
}

void first() {
    int stopAmount, routeAmount, routeNumber, routes[101] = {0};

    scanf("%d", &stopAmount);

    for (int i = 0; i < stopAmount; i++) {
        scanf("%d", &routeAmount);

        for (int j = 0; j < routeAmount; j++) {
            scanf("%d", &routeNumber);

            routes[routeNumber]++;
        }
    }

    for (int i = 1; i < 101; i++) {
        if (routes[i] == stopAmount) {
            printf("%d ", i);
        }
    }
}

void second() {
    int eposideAmount, eposide, sum1 = 0, sum2 = 0;

    scanf("%d", &eposideAmount);

    for (int i = 0; i < eposideAmount - 1; i++) {
        scanf("%d", &eposide);

        sum1 += eposide;
    }

    for (int i = 1; i <= eposideAmount; i++) {
        sum2 += i;
    }

    printf("%d", sum2 - sum1);
}

int compare(const void *a, const void *b) {
    return (*(char *) a - *(char *) b);
}

void third() {
    int requestAmount;
    char string[1001];

    scanf("%d", &requestAmount);

    while (requestAmount--) {
        scanf("%s", string);

        int length = strlen(string);

        qsort(string, length, sizeof(char), compare);

        if (string[0] == string[length - 1]) {
            printf("-1\n");
        } else {
            printf("%s\n", string);
        }
    }
}

void four() {
    int letterCounters[26] = {0}, pupilAmount, minTalkingPupils = 0;
    char pupil_names[21];

    scanf("%d", &pupilAmount);

    while (pupilAmount--) {
        scanf("%s", pupil_names);

        letterCounters[pupil_names[0] - 'a']++;
    }

    for (int i = 0; i < 26; i++) {
        int x = letterCounters[i] / 2;
        int y = (letterCounters[i] + 1) / 2;

        minTalkingPupils += (x * (x - 1) / 2) + (y * (y - 1) / 2);
    }

    printf("%d\n", minTalkingPupils);
}

void five() {
    int ballAmount, peopleAmount, count = 0;
    bool flag = false;

    scanf("%d %d", &ballAmount, &peopleAmount);

    char color_of_ball[ballAmount];

    gets(color_of_ball);
    gets(color_of_ball);

    for (int i = 0; color_of_ball[i]; i++, count = 0) {
        for (int j = 0; color_of_ball[j]; j++) {
            if (color_of_ball[i] == color_of_ball[j]) {
                count++;
            }
        }

        if (count > peopleAmount) {
            flag = true;
        }
    }

    printf(flag ? "NO" : "YES");
}

void six() {
    char string[100001];
    int puppyAmount;
    bool flag = false;

    scanf("%d %s", &puppyAmount, string);

    for (int i = 0; i < puppyAmount; i++) {
        for (int j = i + 1; j < puppyAmount; j++) {
            if (string[i] == string[j]) {
                flag = true;

                break;
            }
        }

        if (flag) {
            break;
        }
    }

    if (puppyAmount == 1) {
        flag = true;
    }

    printf(flag ? "Yes" : "No");
}

void seven() {
    int freeStepAmount, requiredStepAmount;
    char steps[60];

    scanf("%d%d%steps", &freeStepAmount, &requiredStepAmount, steps);

    int rocketSteps[40] = {0};

    for (int i = 0; i < freeStepAmount; i++) {
        rocketSteps[steps[i] - 'a'] = 1;
    }

    int result = 0;

    for (int i = 0; requiredStepAmount && i < 26; i++) {
        if (rocketSteps[i]) {
            requiredStepAmount--;
            i++;
            result += i;
        }
    }

    if (requiredStepAmount) {
        result = -1;
    }

    printf("%d\n", result);
}

void eight() {
    int barAmount = scanInt(), towerAmount = 0, maxHeight = 0;
    int bars[1000] = {0};

    for (int i = 0; i < barAmount; ++i) {
        int towerHeight = ++bars[scanInt() - 1];

        if (towerHeight == 1) {
            towerAmount++;
        }

        if (towerHeight > maxHeight) {
            maxHeight = towerHeight;
        }
    }

    printf("%d %d", maxHeight, towerAmount);
}

int findNextHole(const int* students, int currentStudent) {
    int visited[1001] = {0};

    while (!visited[currentStudent]) {
        visited[currentStudent] = 1;
        currentStudent = students[currentStudent - 1];
    }

    return currentStudent;
}

void nine() {
    int badPupilAmount;

    scanf("%d", &badPupilAmount);

    int *badPupils = (int *) malloc(badPupilAmount * sizeof(int));

    for (int i = 0; i < badPupilAmount; i++) {
        scanf("%d", &badPupils[i]);
    }

    for (int i = 1; i <= badPupilAmount; i++) {
        printf("%d ", findNextHole(badPupils, i));
    }

    free(badPupils);
}

void ten() {
    int stringLength = scanInt(), arr[26] = {}, count = 0;

    char str[stringLength];

    scanf("%s", str);

    if (stringLength > 26) {
        printf("-1");
        return;
    }

    for (int i = 0; i < stringLength; i++) {
        if(++arr[str[i] - 'a'] == 1) {
            count++;
        }
    }

    printf("%d", stringLength - count);
}

int compareChars(const void *a, const void *b) {
    char arg1 = *(const char *) a;
    char arg2 = *(const char *) b;

    if (arg1 < arg2) {
        return -1;
    }

    return arg1 == arg2 ? 0 : 1;
}

int getMinClickAmount(const char* sherlockCard, const char* moriartiCard, int digitAmount) {
    int max = 0;

    for (int i = 0; i < digitAmount; i++) {
        int j = 0;

        while (moriartiCard[i] >= sherlockCard[j] && j < digitAmount) {
            j++;
        }

        if (j > max) {
            max++;
        }
    }

    return digitAmount - max;
}

int getMaxClickAmount(const char* sherlockCard, const char* moriartiCard, int digitsNumber) {
    int max = 0;

    for (int i = 0; i < digitsNumber; i++) {
        int j = 0;

        while (moriartiCard[i] > sherlockCard[j] && j < digitsNumber) {
            j++;
        }

        if (j > max) {
            max++;
        }
    }

    return max;
}

void eleven() {
    int digitAmount;

    scanf("%d\n", &digitAmount);

    char sherlockCardNumber[digitAmount], moriartiCardNumber[digitAmount];

    scanf("%s\n", sherlockCardNumber);
    scanf("%s", moriartiCardNumber);

    qsort(sherlockCardNumber, digitAmount, sizeof(char), compareChars);
    qsort(moriartiCardNumber, digitAmount, sizeof(char), compareChars);

    int minClicks = getMinClickAmount(sherlockCardNumber, moriartiCardNumber, digitAmount);
    int maxClicks = getMaxClickAmount(sherlockCardNumber, moriartiCardNumber, digitAmount);

    printf("%d\n%d", minClicks, maxClicks);
}

void twelve() {
    long long registeredUsers, oppositePairsSum = 0;
    int opposite_count[21] = {0};

    scanf("%lld", &registeredUsers);

    for (int i = 1; i <= registeredUsers; i++) {
        int value = scanInt();

        oppositePairsSum += opposite_count[10 - value];
        opposite_count[value + 10]++;
    }

    printf("%lld", oppositePairsSum);
}